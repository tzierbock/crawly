package main

import (
	"bytes"
	"encoding/csv"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"

	"golang.org/x/net/html"
)

func main() {
	a := args{}

	flag.StringVar(&a.Domain, "domain", "", "domain to crawl")
	flag.StringVar(&a.Start, "start", "/", "page to start crawling from")
	flag.StringVar(&a.Report, "report", "report.csv", "report file to write results to")
	flag.Parse()

	if err := a.Validate(); err != nil {
		log.Fatal("invalid args: ", err)
	}

	if err := run(a); err != nil {
		log.Fatal(err)
	}
}

type args struct {
	Domain string
	Start  string
	Report string
}

func (a args) Validate() error {
	if a.Domain == "" {
		return fmt.Errorf("domain must be a valid url")
	}

	if _, err := url.Parse(a.Domain); err != nil {
		return fmt.Errorf("domain must be a valid url: %w", err)
	}

	if _, err := url.Parse(a.Domain + a.Start); err != nil {
		return fmt.Errorf("start url must be a valid url: %w", err)
	}

	return nil
}

func run(a args) error {

	wg := newCounter()

	linkQueue := make(chan string, 10)
	pageQueue := make(chan page, 10)
	qWG := newCounter()

	linkQueueA, linkQueueB := tee(linkQueue, qWG)
	go processLinks(
		dedupeLinks(
			limitLinksToDomain(a.Domain, linkQueueA, wg),
			wg,
		),
		pageQueue,
		wg,
	)

	go processPages(
		pageQueue,
		linkQueue,
		wg,
	)

	reportQueue := make(chan report, 10)
	go processLinkQuality(
		dedupeLinks(linkQueueB, qWG),
		reportQueue,
		qWG,
	)

	reports := []report{}
	go func() {
		for r := range reportQueue {
			reports = append(reports, r)
		}
	}()

	// start the processing pipeline from the first link
	wg.Add(1)
	linkQueue <- a.Domain + a.Start

	wg.Wait()
	qWG.Wait()

	if a.Report != "" {
		buf := bytes.NewBuffer(nil)
		w := csv.NewWriter(buf)

		for _, r := range reports {
			w.Write([]string{
				r.Rating,
				r.Link,
				strconv.Itoa(r.Status),
			})
		}
		w.Flush()

		if err := w.Error(); err != nil {
			return fmt.Errorf("writing report: %w", err)
		}

		if err := ioutil.WriteFile(filepath.Clean(a.Report), buf.Bytes(), 0644); err != nil {
			return fmt.Errorf("writing report: %w", err)
		}
	}

	return nil
}

type page struct {
	Body   []byte
	Domain string
	Path   string
}

func processLinkQuality(linkQueue chan string, reportQueue chan report, wg *counter) {
	for link := range linkQueue {
		go func(link string) {
			defer wg.Done()
			rep, err := createLinkQualityReport(link)
			if err != nil {
				fmt.Println("quality check:", err)
				return
			}
			reportQueue <- rep
		}(link)
	}
}

type report struct {
	Link   string
	Rating string
	Status int
}

func createLinkQualityReport(link string) (report, error) {
	rep := report{Link: link}
	res, err := http.Get(link)
	if err != nil {
		return report{}, err
	}

	if res.StatusCode >= 300 || res.StatusCode < 200 {
		rep.Rating = "bad status"
		rep.Status = res.StatusCode
	} else {
		rep.Status = res.StatusCode
		rep.Rating = "ok"
	}

	return rep, nil
}

func processLinks(linkQueue chan string, pageQueue chan page, wg *counter) {
	for link := range linkQueue {
		go processLink(link, pageQueue, wg)
	}
}

func processLink(link string, pageQueue chan page, wg *counter) {
	fmt.Println("crawling", link)
	p, err := loadPage(link)
	if err != nil {
		wg.Done()
		fmt.Println(fmt.Errorf("crawling %s: %w", link, err))
		return
	}
	pageQueue <- p
}

func loadPage(u string) (page, error) {
	parts, err := url.Parse(u)
	if err != nil {
		return page{}, err
	}

	res, err := http.Get(u)
	if err != nil {
		return page{}, fmt.Errorf("requesting %s: %w", u, err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return page{}, fmt.Errorf("requesting %s: %s", u, res.Status)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return page{}, err
	}

	return page{
		Body:   body,
		Domain: parts.Scheme + "://" + parts.Host,
		Path:   parts.Path,
	}, nil
}

func processPages(pageQueue chan page, linkQueue chan string, wg *counter) {
	for p := range pageQueue {
		go processPage(p, linkQueue, wg)
	}
}

func processPage(p page, linkQueue chan string, wg *counter) {
	defer wg.Done()
	defer fmt.Println("done:", p.Domain+p.Path)

	links, err := parsePageLinks(p)
	if err != nil {
		fmt.Println(fmt.Errorf("parsing: %w", err))
		return
	}

	for _, link := range links {
		wg.Add(1)
		linkQueue <- link
	}
}

func parsePageLinks(pg page) ([]string, error) {
	p, err := html.Parse(bytes.NewBuffer(pg.Body))
	if err != nil {
		return nil, err
	}

	links := make([]string, 0, 10)

	var f func(n *html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode {
			for _, attr := range n.Attr {
				if attr.Key == "href" && attr.Namespace == "" {
					link := attr.Val
					switch {
					case strings.HasPrefix(link, "javascript:;"):
						continue
					case strings.HasPrefix(link, "#"):
						continue
					case strings.HasPrefix(link, "mailto:"):
						continue
					case strings.HasPrefix(link, "http://") || strings.HasPrefix(link, "https://"):
						// do nothing
					case strings.HasPrefix(link, "/"):
						link = pg.Domain + link
					default:
						link = pg.Domain + pg.Path + link
					}

					links = append(links, link)
				}
			}
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}

	f(p)

	return links, nil
}

func limitLinksToDomain(domain string, in chan string, wg *counter) (out chan string) {
	out = make(chan string, 10)
	go func() {
		for link := range in {
			if strings.HasPrefix(link, domain) {
				out <- link
			} else {
				wg.Done() // done processing this link
			}
		}
	}()
	return out
}

func dedupeLinks(in chan string, wg *counter) (out chan string) {
	out = make(chan string, 10)
	idx := map[string]struct{}{}
	go func() {
		for link := range in {
			_, exists := idx[link]
			if !exists {
				idx[link] = struct{}{}
				out <- link
			} else {
				wg.Done() // done processing this link
			}
		}
	}()
	return out
}

func tee(in chan string, wg *counter) (a, b chan string) {
	a, b = make(chan string, 10), make(chan string, 10)
	go func() {
		for link := range in {
			wg.Add(1)
			a <- link
			b <- link
		}
	}()
	return a, b
}

type counter struct {
	wg *sync.WaitGroup

	count *int64
}

func newCounter() *counter {
	v := int64(0)
	return &counter{
		wg:    &sync.WaitGroup{},
		count: &v,
	}
}

func (c *counter) Add(i int) {
	atomic.AddInt64(c.count, int64(i))
	c.wg.Add(i)
}

func (c *counter) Done() {
	atomic.AddInt64(c.count, -1)
	c.wg.Done()
}

func (c *counter) Count() int {
	return int(*c.count)
}

func (c *counter) Wait() {
	c.wg.Wait()
}
